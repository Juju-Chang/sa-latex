\contentsline {chapter}{\numberline {1}Research Description}{1}
\contentsline {section}{\numberline {1.1}Overview of the Current State of Technology}{1}
\contentsline {section}{\numberline {1.2}Research Objectives}{3}
\contentsline {subsection}{\numberline {1.2.1}General Objectives}{3}
\contentsline {subsection}{\numberline {1.2.2}Specific Objectives}{4}
\contentsline {section}{\numberline {1.3}Scope and Limitations of the Research}{4}
\contentsline {section}{\numberline {1.4}Significance of the Research}{5}
\contentsline {section}{\numberline {1.5}Research Methodology}{6}
\contentsline {section}{\numberline {1.6}Calendar of Activities}{8}
\contentsline {chapter}{\numberline {2}Review of Related Literature}{9}
\contentsline {section}{\numberline {2.1}Rule Based Methods}{9}
\contentsline {section}{\numberline {2.2}Machine Learning}{11}
\contentsline {subsection}{\numberline {2.2.1}Na\"{\i }ve Bayes}{11}
\contentsline {subsection}{\numberline {2.2.2}Neural Network}{13}
\contentsline {subsection}{\numberline {2.2.3}Maximum Entropy}{14}
\contentsline {subsection}{\numberline {2.2.4}Multi-classifier Model}{15}
\contentsline {section}{\numberline {2.3}Rule-Based and Machine Learning}{16}
\contentsline {chapter}{\numberline {3}Research Methodology}{23}
\contentsline {section}{\numberline {3.1}Sentiment Analysis}{23}
\contentsline {subsection}{\numberline {3.1.1}Analyzing Filipino Editorials through Information Extraction and Sentiment Analysis}{25}
\contentsline {section}{\numberline {3.2}Data Annotation}{26}
\contentsline {section}{\numberline {3.3}Machine Learning-based Classifiers}{28}
\contentsline {subsection}{\numberline {3.3.1}Pointwise Mutual Information }{29}
\contentsline {subsection}{\numberline {3.3.2}Na\"{\i }ve Bayes }{30}
\contentsline {subsection}{\numberline {3.3.3}Support Vector Machine }{31}
\contentsline {subsection}{\numberline {3.3.4}Decision Trees}{32}
\contentsline {subsection}{\numberline {3.3.5}Artificial Neural Network }{32}
\contentsline {subsection}{\numberline {3.3.6}K-Nearest Neighbor }{35}
\contentsline {subsection}{\numberline {3.3.7}Maximum Entropy }{36}
\contentsline {section}{\numberline {3.4}Features used by the Classifiers}{37}
\contentsline {subsection}{\numberline {3.4.1}N-grams}{37}
\contentsline {subsection}{\numberline {3.4.2}Terms and their Frequency}{38}
\contentsline {subsubsection}{Term Frequency Model }{38}
\contentsline {subsubsection}{Term Frequency \IeC {\textendash } Inverse Document Frequency}{39}
\contentsline {subsection}{\numberline {3.4.3}Part of Speech}{40}
\contentsline {subsection}{\numberline {3.4.4}Sentence Patterns}{41}
\contentsline {subsection}{\numberline {3.4.5}Bag of Words}{42}
\contentsline {subsection}{\numberline {3.4.6}Lexical Resources}{42}
\contentsline {subsubsection}{AFINN}{43}
\contentsline {subsubsection}{SentiWordNet}{43}
\contentsline {subsubsection}{Semantic Orientation (SO-CAL) Calculator Lexicon }{44}
\contentsline {subsection}{\numberline {3.4.7}Valence Shifters }{44}
\contentsline {subsubsection}{Negations }{45}
\contentsline {subsubsection}{Intensifiers and Diminishers}{45}
\contentsline {subsubsection}{Irrealis Markers}{46}
\contentsline {section}{\numberline {3.5}Classifier Combination}{46}
\contentsline {subsection}{\numberline {3.5.1}First Step}{47}
\contentsline {subsubsection}{Boosting}{47}
\contentsline {subsubsection}{Stacking}{47}
\contentsline {subsection}{\numberline {3.5.2}Second Step}{47}
\contentsline {subsubsection}{N-best}{48}
\contentsline {subsection}{\numberline {3.5.3}Last Step}{48}
\contentsline {subsubsection}{Majority Voting Rule}{48}
\contentsline {subsubsection}{Kittler's Theoretical Framework}{48}
\contentsline {section}{\numberline {3.6}Evaluation Techniques}{52}
\contentsline {subsection}{\numberline {3.6.1}Fundamental numbers}{52}
\contentsline {subsection}{\numberline {3.6.2}Precision}{52}
\contentsline {subsection}{\numberline {3.6.3}Recall}{53}
\contentsline {subsection}{\numberline {3.6.4}F-score}{53}
\contentsline {subsection}{\numberline {3.6.5}Accuracy}{53}
\contentsline {subsection}{\numberline {3.6.6}Fleiss Kappa}{53}
\contentsline {chapter}{\numberline {4}Research Framework}{55}
\contentsline {section}{\numberline {4.1} Development of the Corpus and Lexical Resources}{56}
\contentsline {subsection}{\numberline {4.1.1}Gathering the New Corpus and Annotation}{56}
\contentsline {subsection}{\numberline {4.1.2}Pre-processing and Filtering of the Corpus}{59}
\contentsline {subsection}{\numberline {4.1.3}Creation of the Lexical Resources}{59}
\contentsline {section}{\numberline {4.2}Feature Extraction}{62}
\contentsline {section}{\numberline {4.3}Feature Selection}{64}
\contentsline {section}{\numberline {4.4}Classifiers}{68}
\contentsline {subsection}{\numberline {4.4.1}Rule-based Classifier}{68}
\contentsline {subsection}{\numberline {4.4.2}Machine Learning-Based Classifiers}{69}
\contentsline {subsection}{\numberline {4.4.3}Result Aggregation}{70}
\contentsline {chapter}{\numberline {5}Results and Analysis}{71}
\contentsline {section}{\numberline {5.1}Annotation}{71}
\contentsline {section}{\numberline {5.2}Baseline Results}{78}
\contentsline {section}{\numberline {5.3}Actual Test Results}{80}
\contentsline {subsection}{\numberline {5.3.1}Rule-based Classifier}{81}
\contentsline {subsection}{\numberline {5.3.2}Machine Learning-Based Classifiers}{83}
\contentsline {subsection}{\numberline {5.3.3}Result Aggregation}{89}
\contentsline {chapter}{\numberline {6}Conclusions and Recommendations}{91}
\contentsline {chapter}{Appendices}{100}
\contentsline {chapter}{\numberline {A}Articles}{101}
\contentsline {section}{\numberline {A.1}3:1 and 2:2}{101}
\contentsline {subsection}{\numberline {A.1.1}35 Batang babae, nakitang patay at may 'di maipaliwanag na mga sugat na gawa raw ng aswang}{101}
\contentsline {subsection}{\numberline {A.1.2}38 1, nasawi, 3, sugatan sa sunog sa Pagadian city}{103}
\contentsline {subsection}{\numberline {A.1.3}47 Sinasabing Chinese fugitive na si Wang Bo, nasa kostudiya pa rin daw ng BI}{104}
\contentsline {subsection}{\numberline {A.1.4}55 Warrant of arrest, inilabas na laban sa EDSA \IeC {\textquoteleft }hulidap\IeC {\textquoteright } cops; ilang suspek, pinaghahanap pa}{106}
\contentsline {subsection}{\numberline {A.1.5}159 3 anggulo, sinisilip ng pulisya na motibo sa pagpatay sa negosyante sa Laguna}{108}
\contentsline {subsection}{\numberline {A.1.6}169 Ronnie Ricketts, nagpiyansa sa kinakaharap na kasong katiwalian}{109}
\contentsline {subsection}{\numberline {A.1.7}187 Makati Mayor Junjun Binay pansamantalang bumaba sa puwesto}{110}
\contentsline {subsection}{\numberline {A.1.8}198 Umano'y pekeng bigas, nakarating na nga ba sa Metro Manila?}{112}
\contentsline {subsection}{\numberline {A.1.9}696 Pagdinig sa kaso ng One Dream scam, aarangkada sa Huwebes}{113}
\contentsline {subsection}{\numberline {A.1.10}706 Higit P4-B \textsc {\char 13}congressional insertion\textsc {\char 13} sa 2015 budget, bubusisiin}{115}
\contentsline {section}{\numberline {A.2}4 In agreemnet}{116}
\contentsline {subsection}{\numberline {A.2.1}154 Interesado ang DND sa P-3C Orion spy plane ng Japan}{116}
\contentsline {subsection}{\numberline {A.2.2}165 LOOK: Libreng operasyon sa puso ng 17-anyos na Pinoy na isinagawa sa Africa, naging matagumpay}{117}
\contentsline {subsection}{\numberline {A.2.3}173 Si Grace Poe ang magpapasya kung tatakbo sa 2016 polls, ayon kay Susan Roces}{118}
\contentsline {subsection}{\numberline {A.2.4}179 Comelec: Bidding para isaayos ang mga PCOS machine, bigo}{119}
\contentsline {subsection}{\numberline {A.2.5}185 10-anyos na lalaki, patay matapos mabundol ng tricycle sa Pangasinan}{121}
\contentsline {subsection}{\numberline {A.2.6}252 Mahirap na batang estudyante, nagsauli ng napulot na pera}{122}
\contentsline {subsection}{\numberline {A.2.7}339 Escudero, 'di nagsisisi sa pag-endorso noong kay Binay; pero mayroon daw siyang natutunan}{123}
\contentsline {subsection}{\numberline {A.2.8}352 ``Puna ng UN official Rehabilitasyon sa mga lugar na sinalanta ni \textsc {\char 13}Yolanda,\textsc {\char 13} mabagal"}{125}
\contentsline {subsection}{\numberline {A.2.9}355 Lagay ng bansa sa pananaw ni VP Binay, malalaman sa Lunes}{127}
\contentsline {subsection}{\numberline {A.2.10}380 ``Paningin lang ang nawala, 'di ang pangarap Grade 6 na bulag, pursigidong makatapos ng pag-aaral"}{128}
\contentsline {section}{\numberline {A.3}4 In agreemnet}{129}
\contentsline {subsection}{\numberline {A.3.1}11 Ano ang dapat gawin ng Pilipinas sa girian ng US at China kaugnay ng West PHL Sea?}{129}
\contentsline {subsection}{\numberline {A.3.2}110 Talumpati ni Pangulong Aquino sa decommissioning ng mga miyembro ng MILF at paglalatag ng kanilang mga armas}{131}
\contentsline {subsection}{\numberline {A.3.3}363 19-anyos na pumatay sa kanyang lola at kumain sa puso ng biktima, kinasuhan na}{136}
\contentsline {subsection}{\numberline {A.3.4}374 Mga sundalo ng US, limitado ang galaw habang nasa Subic, Zambales}{137}
\contentsline {chapter}{References}{139}
