\chapter{Conclusion and Recommendations}
\section{Conclusion}
	The difficulty of human sentiment analysis lies in the preconceived ideas each person has regarding various issues and matters. The inherent bias humans have towards events cause them to interpret news articles differently. Even in the presence of guidelines in annotation, haphazard analysis of the text and understanding of the guidelines will still produce ambiguous results. To remove the bias in interpretation present in all human beings, annotators run the risk of dismissing most news articles as having neutral sentiment, disregarding the opinionated content the author has placed in the article. 

	There are also cases when the article contains multiple opinionated sentiments, thus causing it to have an ambiguous overall sentiment. This ambiguity causes human sentiment analyzers to produce varying interpretations based on their readings of the text. 

	The results produced by the classifiers using all features show that bi-grams are not so effective in the classification of news articles. N-grams that have n values higher than 2 however, show promising potential in the categorization of texts. Though this feature takes a long time to extract because of its ability to generate plentiful attributes, it is very useful in the analysis of texts where the terms within the texts are integral to the text’s classification. 

	For the negation feature, bigrams with negators attached to them perform the best. This can be explained by the characteristic of the Filipino negators. Since negators are most often immediately followed by the part of speech that it is modifying, most relevant negations can already be captured using a window size of 2. Using an n-gram size of 3 will cause the negation feature to be too infrequently occurring and specific to only a select number of articles. Using features that tailor too well to the training dataset will cause overfitting, and will produce a model that performs poorly on any dataset besides its training set. With the exception of a few matches like ‘hindi siya’, ‘hindi sila’, ‘hindi ako’, etc., most negations of length 2 extracted already have sufficient capacity in order to aid to the classification of texts.

	Sentence pattern as a feature proved to be ineffective because the study only considered patterns of length 2 to 5, which may have been too short to hold any significant classification power. Also, news articles are usually constructed using a certain format; thus, considering the sentence patterns of articles may not prove to be too useful in the categorization of articles.

	Furthermore, extracting the sentence pattern feature requires a part-of-speech tagger that has been previously trained to process Filipino sentences. The errors caused by the inaccuracy of the sentences tagged caused results of the succeeding classifiers to also worsen. An added complexity is the need for the article to be represented in another form in order to check for the relevant sentence patterns that are present within it. Unlike the other features that are term-based, the sentence pattern feature requires that the whole article be transformed into its corresponding tagged form first, before feature extraction can take place.

	Once features are extracted, the resulting data set is also large in scale, thus causing classifiers like the Artificial Neural Network and the Maximum Entropy to take a long time in producing a classifier model.

	Even though the TF-IDF feature commonly takes a while to be processed by the classifiers due to the amount of features it is able to extract given a certain configuration, it is a feature that is easily extracted. The implementation of the TF-IDF feature is very similar to that of unigrams, with the exception of the computation needed in order to determine the words to be included in the TF-IDF list. Unlike the typical n-gram where a minimum number of occurrences set by the author must first be satisfied in order for the n-gram to appear in the list of features extracted, the TF-IDF populates its list by not only considering frequently occurring words, but also those which hold great classification power. Thus, this feature is able to perform excellently in the classification of articles where great importance is placed upon the terms present in the article. Also, once a sufficiently large portion of the TF-IDF has already been taken in order to come up with the bag of words to use, the BoW can easily be reduced further by taking a percentage of its contents and coming up with a scaled down BoW.

	Also, the study disregarded the presence of irrealis markers in texts, but analysis of the articles causing annotator disagreement has shown that the author also selects which direct quotations to include in the article depending on the sentiment that he or she wants to frame. Therefore, the direct quotations found in articles can also contribute to the classification process.

	\begin{landscape}
	\begin{table}[ht!]
		\centering
		\caption{Results of all classifiers and aggregators using the test set of Cagampan.}
		\label{my-label}
		\resizebox{1.3\textwidth}{!}{
		\begin{tabular}{ll|l|l|l|l|l|l|l|l|l|}
		\cline{3-11}
		                                                &                               & \multicolumn{3}{c|}{F-score}                                                    & \multicolumn{3}{c|}{Recall}                                                     & \multicolumn{3}{c|}{Precision}                                                  \\ \hline
		\multicolumn{1}{|c|}{Classifier}                & \multicolumn{1}{c|}{Accuracy} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} \\ \hline
		\rowcolor{lightgray}
		\multicolumn{1}{|l|}{SO-CAL}                    & 41.7949\%                     & 0.4615                   & 0.5230                   & 0.1132                    & 0.4846                   & 0.7000                   & 0.0692                    & 0.4406                   & 0.4174                   & 0.3103                    \\ \hline
		\multicolumn{1}{|l|}{Weighted Majority}         & 39.2308\%                     & 0.0986                   & 0.3585                   & 0.5070                    & 0.0538                   & 0.2923                   & 0.8308                    & 0.5833                   & 0.4634                   & 0.3649                    \\ \hline
		\multicolumn{1}{|l|}{Term Frequency Model}      & 37.9487\%                     & 0.1014                   & 0.3429                   & 0.5038                    & 0.0538                   & 0.3231                   & 0.7615                    & 0.8750                   & 0.3652                   & 0.3764                    \\ \hline
		\multicolumn{1}{|l|}{Naïve Bayes}               & 37.1795\%                     & 0.1412                   & 0.2762                   & 0.5035                    & 0.0923                   & 0.1923                   & 0.8308                    & 0.3000                   & 0.4902                   & 0.3612                    \\ \hline
		\multicolumn{1}{|l|}{K-Nearest Neighbor}        & 36.1538\%                     & 0.3045                   & 0.3465                   & 0.4240                    & 0.2846                   & 0.3385                   & 0.4615                    & 0.3274                   & 0.3548                   & 0.3922                    \\ \hline
		\multicolumn{1}{|l|}{Aggregated Majority}       & 35.1282\%                     & 0.0296                   & 0.2963                   & 0.4965                    & 0.0154                   & 0.2154                   & 0.8231                    & 0.4000                   & 0.4746                   & 0.3555                    \\ \hline
		\multicolumn{1}{|l|}{Stacking}                  & 34.3590\%                     & 0.2045                   & 0.0000                   & 0.4895                    & 0.1385                   & 0.0000                   & 0.8923                    & 0.3913                   & 0.0000                   & 0.3372                    \\ \hline
		\multicolumn{1}{|l|}{Artificial Neural Network} & 34.1026\%                     & 0.0146                   & 0.4451                   & 0.3704                    & 0.0077                   & 0.5923                   & 0.4231                    & 0.1429                   & 0.3565                   & 0.3293                    \\ \hline
		\multicolumn{1}{|l|}{Support Vector Machine}    & 33.5897\%                     & 0.0000                   & 0.0444                   & 0.4971                    & 0.0000                   & 0.0231                   & 0.9846                    & 0.0000                   & 0.0000                   & 0.3325                    \\ \hline
		\multicolumn{1}{|l|}{Majority}                  & 33.5897\%                     & 0.0571                   & 0.3030                   & 0.4987                    & 0.0308                   & 0.2308                   & 0.7462                    & 0.4000                   & 0.4412                   & 0.3745                    \\ \hline
		\end{tabular}}
	\end{table}
	\end{landscape}

	The SO-CAL classifier has shown good results even using an imbalanced training set. Though the classifier may have a bias towards opinionated predictions, it does not require any training prior to testing, and has settings that can be easily changed in order to improve results. A weakness this classifier has lies in the fact that it heavily relies on the weights assigned to the sentiment terms. For the weights of these sentiment terms to be correct, validation from a linguist is necessary. Furthermore, the accuracy of translation of the words within the corpus must also be validated by a linguist for most optimal performance.
	
	Between the 2 rule-based methods available, the Term Frequency Model is the easier one to implement. On top of not requiring a large dataset in order to come up with acceptable results, the TMF also does not require lexicon construction or result validation from a linguist. However, for good results to be produced by the TMF, a well annotated and balanced training set is necessary. The TMF will surely be useful in the annotations of texts having unique words that define the nature of the texts’ respective category.
	
	The rule-based classifiers performed the best out of all the classifying algorithms implemented because they were relatively unaffected by the difference of the training and test set.
	
	The Na\"{\i}ve Bayes classifier is an incremental learning algorithm that can create a classification model in a short amount of time, even while using a large dataset. It is also the best performing classifier amongst all machine learning algorithms experimented on in this study. The classifier works well with features that extract the terms essential in the classification of an object to a certain category. In the case of news articles where sentiment targets can greatly determine the overall sentiment, an algorithm like the Na\"{\i}ve Bayes classifier which treats features as independent of each other can prove to be very efficient. Additionally, the results of this classifier reflect the quality of the training set it is given.
	
	The Maximum Entropy classifier operates similarly to the Na\"{\i}ve Bayes classifier in concept, but the Maximum Entropy classifier runs a higher risk of overfitting data. This classifier also works well with term-based models, but takes a long time to come up with a training model.
	
	Even though the Support Vector Machine classifier is able to produce a training model in a short amount of time, it is not capable of producing good results for texts that have similar structures, but varying contexts. Since the algorithm is akin to clustering, it only groups objects into categories by considering their similarities. Once the context of these features change, the Support Vector Machine starts producing poor results. It can be concluded therefore, that the SVM classifier is most efficient in the classification of objects that have distinct characteristics that do not vary regardless of the context.
	
	The Artificial Neural Network also takes time in building its classification model, but can produce good results given a good and balanced training set. Since the Artificial Neural Network comes up with a classification model by mimicking the neurons found inside the brain, it will work best with categories that are non-linear. Thus, this classifier is especially useful in the classification of texts that contain words which may have multiple literal and implied meanings. An example of such texts are news articles and product reviews.
	
	Much like the ANN, the K-Nearest Neighbor also relies on a good training set in order to produce good results; except that the KNN is able to create a classification model in a much shorter span of time. The KNN however, does not possess the same power of non-linear classification that the ANN does; for the KNN does not create a computational model, but only assigns objects to different categories based on their proximity or similarity. This kind of classification algorithm is most useful in the categorization of texts having definite patterns like those found in spam e-mails. 
	
	Though an aggregation of results was proposed in order to address the ambiguous nature of news articles, for the issue of ambiguity to be properly addressed, optimal results from the individual classifiers must first be assured. Combining underperforming classifiers with acceptable classifiers towards the task of text categorization will only be equivalent to adding noise to clean data.
	
	It is very important to assign correct weights to the classifiers depending on accuracy, for assigning a value with small increments to the weights of the classifiers will cause all classifiers to have almost equal classification power.
	
	Classification can be accurately done by a single classifier, as long as it sufficiently caters to the characteristics of the data and is given a good feature set that is able to capture the various elements present in the objects being categorized.
	
	Observations regarding the Filipino language and the structure of news articles have to be made in order to make clear connections with the results produced by the study:
	
	\begin{enumerate}
		\item Sentences in news articles follow a certain pattern that aims to maximize the amount of information expressed in the most concise way possible (Parks, 2009).
		\item Negations are context sensitive, and bigrams attached with negators should be used since Filipino negations commonly span 2 words long only.
		\item The terms contained in the BoW and extracted by the n-grams are commonly personalities, issues, and events.
		\item Words used the articles having positive sentiment vary greatly from the words used in articles bearing negative sentiment.
	\end{enumerate}

	Therefore, we can assume that the extraction of sentence patterns is ineffective because repeating patterns in articles of varying sentiment may exist. 


\section{Recommendations}
	In regards to the problem of subjectivity and ambiguity in the sentiment analysis of news articles, the proponents recommend future researchers to determine the general sentiment of the people towards certain topics and issues by coming up with a distribution of sentiment per relevant topic or issue. By aggregating all the sentiments of the articles under a certain topic and making a distribution of sentiment categories per relevant issue or topic (e.g. 80\% of the general public perceive the issue positively and 10\% of the masses have a negative opinion toward it, while the remaining 10\% are neutral), interested parties will be made aware of the opinion of the masses towards various recent events.

	As much as possible, the training and test set to be used must not have words varying greatly in context. The difference in context poses problems to the extracted negation features and will cause the said feature to be useless in classification. The variance in annotation caused by the difference in the nature of texts taken from various sources also creates confusion in the prediction of the classifiers. 

	Since news articles mostly contain relevant terms that pertain to current issues, the BoW and n-grams must be updated in order to classify articles within a certain time period. The features that are most effective in the analysis of news articles are those that capture the relevant terms present in the article, and it can be concluded that in the case of features capturing the terms present in the articles, a wide array of articles from various news categories must be obtained in order to produce a sentiment analyzer that is capable of classifying different articles. Through the use of a more diverse training set, the extracted features will be able to cover more terms that can be useful in the analysis of articles with more specialized content and news topics. Therefore, succeeding researchers can opt to create a sentiment analyzer that is able to learn on future input data by populating the feature set of the system and retraining the models for every time it is given a unique word as input.

	Since the terms in the articles are able to provide great classification power, classifiers that work well with term-based features, such as the Na\"{\i}ve Bayes and Maximum Entropy classifier, must be used. 

	The collection of a balanced dataset is very difficult in the case of articles requiring annotation; therefore, classifiers that rely too much on the quality of the training set should be avoided, such as the SVM and ANN. 

	In the case of news articles, even the words used to frame positive sentiment in an article vary from those used to emphasize the negative sentiment in an article; therefore, rule-based classifiers maximizing on word variance can be utilized in order to produce good results.

	For data gathering, more web articles from various news categories can be considered in order to provide a more diverse corpus. A large dataset can also assure that when data set balancing is performed, a significant portion of the corpus will still remain. 

	With regards to improving the Kappa, the proponents recommend that future researchers stress the importance of following the annotation guidelines given to their respective annotators. It is also important to conduct follow up assessments on the annotators in order to maintain the quality of the annotations they produce. 

	Lastly, it is advisable to have the annotators hired to have a background in linguistics since they are able to more accurately identify the nuances within the sentences, and thus give a more accurate annotation.

	The current sentiment analyzer built can still be improved through the consideration of other methods and aspects such as the extraction of more features, and the consideration of more classifiers. Furthermore, data preprocessing can be improved through the use of more accurate data filters and through the addition of more filter rules. 

	Feature selection can still be greatly enhanced through the use of a different evaluator and search method for the WEKA tool in determining the most appropriate feature/s to select can also aid in the improvement of results. 

	In order to improve the current SO-CAL, the researchers recommend that future researchers employ the help of a linguist in translating the SO-CAL into Filipino instead of simply relying on an online translator and dictionary.

	Furthermore, if a word is not found in the lexicon\textsc{\char13}s specific word list (nouns, verbs, adjectives, etc.) one of its synonyms may be used instead to give it an appropriate weight instead of giving the word a weight of 0.

	The current SO-CAL program was designed in such a way that it is more inclined towards making opinionated predictions than neutral forecasts. Instead, it is more appropriate to have a range of values in place in such a way that if an article possess a value falling in the aforementioned range, it is classified as neutral. Several tests need to be conducted in order to determine the appropriate range to be used for each classification.

	Lastly, aggregation of results should only be done if the individual classifiers are able to reach a certain threshold of results produced. In this case, other methods to aggregate predictions can be experimented upon.