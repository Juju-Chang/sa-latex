\contentsline {chapter}{\numberline {1}Research Description}{1}
\contentsline {section}{\numberline {1.1}Overview of the Current State of Technology}{1}
\contentsline {section}{\numberline {1.2}Research Objectives}{4}
\contentsline {subsection}{\numberline {1.2.1}General Objectives}{4}
\contentsline {subsection}{\numberline {1.2.2}Specific Objectives}{4}
\contentsline {section}{\numberline {1.3}Scope and Limitations of the Research}{4}
\contentsline {section}{\numberline {1.4}Significance of the Research}{6}
\contentsline {section}{\numberline {1.5}Research Methodology}{6}
\contentsline {section}{\numberline {1.6}Calendar of Activities}{10}
\contentsline {chapter}{\numberline {2}Review of Related Literature}{11}
\contentsline {section}{\numberline {2.1}Rule Based Methods}{11}
\contentsline {section}{\numberline {2.2}Machine Learning}{13}
\contentsline {subsection}{\numberline {2.2.1}Na\"{\i }ve Bayes}{13}
\contentsline {subsection}{\numberline {2.2.2}Neural Network}{15}
\contentsline {subsection}{\numberline {2.2.3}Maximum Entropy}{16}
\contentsline {subsection}{\numberline {2.2.4}Multi-classifier Model}{17}
\contentsline {section}{\numberline {2.3}Rule-Based and Machine Learning}{18}
\contentsline {chapter}{\numberline {3}Research Methodology}{26}
\contentsline {section}{\numberline {3.1}Sentiment Analysis}{26}
\contentsline {subsection}{\numberline {3.1.1}Analyzing Filipino Editorials through Information Extraction and Sentiment Analysis}{28}
\contentsline {section}{\numberline {3.2}Data Annotation}{30}
\contentsline {section}{\numberline {3.3}Machine Learning-based Classifiers}{32}
\contentsline {subsection}{\numberline {3.3.1}Pointwise Mutual Information }{32}
\contentsline {subsection}{\numberline {3.3.2}Na\"{\i }ve Bayes }{34}
\contentsline {subsection}{\numberline {3.3.3}Support Vector Machine }{35}
\contentsline {subsection}{\numberline {3.3.4}Decision Trees}{36}
\contentsline {subsection}{\numberline {3.3.5}Artificial Neural Network }{37}
\contentsline {subsection}{\numberline {3.3.6}K-Nearest Neighbor }{40}
\contentsline {subsection}{\numberline {3.3.7}Maximum Entropy }{40}
\contentsline {section}{\numberline {3.4}Features used by the Classifiers}{42}
\contentsline {subsection}{\numberline {3.4.1}N-grams}{42}
\contentsline {subsection}{\numberline {3.4.2}Terms and their Frequency}{43}
\contentsline {subsubsection}{Term Frequency Model }{43}
\contentsline {subsubsection}{Term Frequency \IeC {\textendash } Inverse Document Frequency}{44}
\contentsline {subsection}{\numberline {3.4.3}Part of Speech}{45}
\contentsline {subsection}{\numberline {3.4.4}Sentence Patterns}{47}
\contentsline {subsection}{\numberline {3.4.5}Bag of Words}{47}
\contentsline {subsection}{\numberline {3.4.6}Lexical Resources}{48}
\contentsline {subsubsection}{AFINN}{48}
\contentsline {subsubsection}{SentiWordNet}{49}
\contentsline {subsubsection}{Semantic Orientation (SO-CAL) Calculator Lexicon }{50}
\contentsline {subsection}{\numberline {3.4.7}Valence Shifters }{50}
\contentsline {subsubsection}{Negations }{51}
\contentsline {subsubsection}{Intensifiers and Diminishers}{51}
\contentsline {subsubsection}{Irrealis Markers}{52}
\contentsline {section}{\numberline {3.5}Classifier Combination}{53}
\contentsline {subsection}{\numberline {3.5.1}First Step}{53}
\contentsline {subsubsection}{Boosting}{53}
\contentsline {subsubsection}{Stacking}{53}
\contentsline {subsection}{\numberline {3.5.2}Second Step}{54}
\contentsline {subsubsection}{N-best}{54}
\contentsline {subsection}{\numberline {3.5.3}Last Step}{54}
\contentsline {subsubsection}{Majority Voting Rule}{55}
\contentsline {subsubsection}{Kittler's Theoretical Framework}{55}
\contentsline {section}{\numberline {3.6}Evaluation Techniques}{58}
\contentsline {subsection}{\numberline {3.6.1}Fundamental numbers}{58}
\contentsline {subsection}{\numberline {3.6.2}Precision}{59}
\contentsline {subsection}{\numberline {3.6.3}Recall}{59}
\contentsline {subsection}{\numberline {3.6.4}F-score}{59}
\contentsline {subsection}{\numberline {3.6.5}Accuracy}{60}
\contentsline {subsection}{\numberline {3.6.6}Fleiss Kappa}{60}
\contentsline {chapter}{\numberline {4}Research Framework}{62}
\contentsline {section}{\numberline {4.1}Development of the Corpus and Lexical Resources}{62}
\contentsline {subsection}{\numberline {4.1.1}Gathering the New Corpus and Annotation}{62}
\contentsline {subsection}{\numberline {4.1.2}Pre-processing and Filtering of the Corpus}{67}
\contentsline {subsection}{\numberline {4.1.3}Creation of the Lexical Resources}{67}
\contentsline {section}{\numberline {4.2}Feature Extraction}{70}
\contentsline {section}{\numberline {4.3}Feature Selection}{72}
\contentsline {section}{\numberline {4.4}Classifiers}{77}
\contentsline {subsection}{\numberline {4.4.1}Rule-based Classifier}{77}
\contentsline {subsection}{\numberline {4.4.2}Machine Learning-Based Classifiers}{79}
\contentsline {subsection}{\numberline {4.4.3}Result Aggregation}{79}
\contentsline {chapter}{\numberline {5}Results and Analysis}{81}
\contentsline {section}{\numberline {5.1}Annotation}{81}
\contentsline {section}{\numberline {5.2}Baseline Results}{90}
\contentsline {section}{\numberline {5.3}Actual Test Results}{93}
\contentsline {subsection}{\numberline {5.3.1}Rule-based Classifier}{93}
\contentsline {subsection}{\numberline {5.3.2}Machine Learning-Based Classifiers}{94}
\contentsline {subsection}{\numberline {5.3.3}Result Aggregation}{99}
\contentsline {subsection}{\numberline {5.3.4}Comparison to Cagampan's Framework}{101}
\contentsline {chapter}{\numberline {6}Conclusion and Recommendations}{105}
\contentsline {section}{\numberline {6.1}Conclusion}{105}
\contentsline {section}{\numberline {6.2}Recommendations}{111}
\contentsline {chapter}{Appendices}{114}
\contentsline {chapter}{\numberline {A}Articles}{115}
\contentsline {section}{\numberline {A.1}3:1 and 2:2}{115}
\contentsline {subsection}{\numberline {A.1.1}Batang babae, nakitang patay at may 'di maipaliwanag na mga sugat na gawa raw ng aswang}{115}
\contentsline {subsection}{\numberline {A.1.2}1, nasawi, 3, sugatan sa sunog sa Pagadian city}{117}
\contentsline {subsection}{\numberline {A.1.3}Sinasabing Chinese fugitive na si Wang Bo, nasa kostudiya pa rin daw ng BI}{118}
\contentsline {subsection}{\numberline {A.1.4}Warrant of arrest, inilabas na laban sa EDSA \IeC {\textquoteleft }hulidap\IeC {\textquoteright } cops; ilang suspek, pinaghahanap pa}{120}
\contentsline {subsection}{\numberline {A.1.5}3 anggulo, sinisilip ng pulisya na motibo sa pagpatay sa negosyante sa Laguna}{122}
\contentsline {subsection}{\numberline {A.1.6}Ronnie Ricketts, nagpiyansa sa kinakaharap na kasong katiwalian}{123}
\contentsline {subsection}{\numberline {A.1.7}Makati Mayor Junjun Binay pansamantalang bumaba sa puwesto}{125}
\contentsline {subsection}{\numberline {A.1.8}Umano'y pekeng bigas, nakarating na nga ba sa Metro Manila?}{127}
\contentsline {subsection}{\numberline {A.1.9}Pagdinig sa kaso ng One Dream scam, aarangkada sa Huwebes}{128}
\contentsline {subsection}{\numberline {A.1.10}Higit P4-B \textsc {\char 13}congressional insertion\textsc {\char 13} sa 2015 budget, bubusisiin}{130}
\contentsline {section}{\numberline {A.2}All Annotators in Disagreement}{131}
\contentsline {subsection}{\numberline {A.2.1}Ano ang dapat gawin ng Pilipinas sa girian ng US at China kaugnay ng West PHL Sea?}{131}
\contentsline {subsection}{\numberline {A.2.2}Talumpati ni Pangulong Aquino sa decommissioning ng mga miyembro ng MILF at paglalatag ng kanilang mga armas}{134}
\contentsline {subsection}{\numberline {A.2.3}19-anyos na pumatay sa kanyang lola at kumain sa puso ng biktima, kinasuhan na}{139}
\contentsline {subsection}{\numberline {A.2.4}Mga sundalo ng US, limitado ang galaw habang nasa Subic, Zambales}{140}
\contentsline {section}{\numberline {A.3}4 In agreement}{142}
\contentsline {subsection}{\numberline {A.3.1}Interesado ang DND sa P-3C Orion spy plane ng Japan}{142}
\contentsline {subsection}{\numberline {A.3.2}LOOK: Libreng operasyon sa puso ng 17-anyos na Pinoy na isinagawa sa Africa, naging matagumpay}{143}
\contentsline {subsection}{\numberline {A.3.3}Si Grace Poe ang magpapasya kung tatakbo sa 2016 polls, ayon kay Susan Roces}{144}
\contentsline {subsection}{\numberline {A.3.4}Comelec: Bidding para isaayos ang mga PCOS machine, bigo}{145}
\contentsline {section}{\numberline {A.4}All in Agreement}{147}
\contentsline {subsection}{\numberline {A.4.1}10-anyos na lalaki, patay matapos mabundol ng tricycle sa Pangasinan}{147}
\contentsline {subsection}{\numberline {A.4.2}Mahirap na batang estudyante, nagsauli ng napulot na pera}{149}
\contentsline {subsection}{\numberline {A.4.3}Escudero, 'di nagsisisi sa pag-endorso noong kay Binay; pero mayroon daw siyang natutunan}{150}
\contentsline {subsection}{\numberline {A.4.4}``Puna ng UN official Rehabilitasyon sa mga lugar na sinalanta ni \textsc {\char 13}Yolanda,\textsc {\char 13} mabagal"}{152}
\contentsline {subsection}{\numberline {A.4.5}Lagay ng bansa sa pananaw ni VP Binay, malalaman sa Lunes}{155}
\contentsline {subsection}{\numberline {A.4.6}``Paningin lang ang nawala, 'di ang pangarap Grade 6 na bulag, pursigidong makatapos ng pag-aaral"}{156}
\contentsline {chapter}{References}{157}
