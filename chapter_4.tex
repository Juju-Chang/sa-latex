\chapter{Research Framework}
    As shown in \figref{fig:sysarchi}, , the study will be divided into two phases. The first is the training phase, followed by the execution or deployment phase. During the training phase, multiple classifiers will be independently trained using the different feature sets. 
    
    \begin{figure} [h!]
       \includegraphics[width=1.0\textwidth]{sysarchi.jpg}      
       \caption{The modules under the Training Phase (left) and the modules under the Execution Phase (right).}
        \label{fig:sysarchi}
    \end{figure}
    
\section{Development of the Corpus and Lexical Resources}
\subsection{Gathering the New Corpus and Annotation}
	For the corpus of the system, only news articles written in Filipino were considered. Articles from the websites of DZMM and GMA News were gathered daily from May 17, 2015 to August 25, 2015, accumulating to 800 articles in total. All articles gathered were extracted together with the date it was published and its title. Moreover, images and videos included in the news articles were not collected as they are beyond the scope of the study. An additional 390 articles were taken from the corpus used by \citeauthor{cagampan2015} \citeyear{cagampan2015} in her study to achieve a total of 1190 articles for the corpus. Since the study will be focusing on improving the results produced by \citeauthor{cagampan2015}\textsc{\char13}s framework, the 390 articles taken will be used in order to validate if the study\textsc{\char13}s framework is an improvement of the one produced by Cagampan. 10-fold cross validation and percentage split will be used to divide the corpus into training and testing sets.

	Three different annotators will be tasked to manually classify each of the 800 articles from DZMM and GMA News in order to get each article\textsc{\char13}s sentiment (i.e. positive, negative or neutral). Each annotator will be given a spreadsheet containing a copy of the articles. The annotators will be asked to annotate separately from one another.

	By definition, articles with positive sentiment are those which:
	\begin{enumerate}
	\item Contain reports regarding the worsening or of an implied worsening of a previously reported issue/event and contain opinions from the author supporting the positivity of the event.
	\item Contain reports regarding the compromise of public safety and supporting opinionated statements from the author
	\item Have more negative ideas than positive ones that are coming from the author
	\item Have negative ideas from the author which weigh more than the positive ideas
	\item Are written negatively (in a condemning, gloomy, distressed and fearful tone) by the author

	\end{enumerate}
	On the other hand, articles with negative sentiment are defined as those which:
	\begin{enumerate}
		\item Contain reports regarding the worsening of a previously reported issue/event
		\item Contain reports regarding the implied worsening of a previously reported issue/event
		\item Contain reports regarding the compromise of public safety
		\item Contain reports regarding that are destructive to the country
		\item Have more negative ideas than positive ones
		\item Have negative ideas which weigh more than the positive ideas
		\item Are written negatively (in a condemning, gloomy, distressed and fearful tone)
	\end{enumerate}
	
	Lastly, neutral articles are those which:
	\begin{enumerate}
		\item Neither have positive nor negative sentiment
	\end{enumerate}

	According to the definitions provided above, an article only becomes opinionated if and only if the author has written the article in such a way as to frame a certain sentiment. Since news articles are objective in nature, author-based sentiment analysis will be the main focus of the study.
	
	Guidelines were provided in order to assist the annotators in tagging articles based on the definition given above. Most of the rules included were taken from the guidelines devised by  \citeauthor{balahur2009rethinking} \citeyear{balahur2009rethinking} in their study as discussed in section 3.2 regarding data annotation. Rules taken from their study are as follows:
	
	\begin{enumerate}
		\item Do not make use of your world knowledge, such as the political views of the entity. If you cannot decide without knowing the political views of the entity, leave it as neutral.

		\item Do not focus in knowing whether the whole piece of text is positive or negative, but exclusively the sentiment expressed towards the entity. If there exist multiple entities, refer to the one referenced in the article title.

		\item Another hint to identify subjectivity: news items whose content cannot be verified or expressly changed to induce a negative/positive opinion should be annotated as positive or negative. E.g. ``Negotiations with Turkey have been delayed" – factual (objective from a sentiment point of view, neutral sentiment). ``EU stalls negotiations with Turkey" – (subjective, negative sentiment).

		\item Note that, in the text snippet ``X supported Y for criticizing Z", there is negative sentiment towards Z from X and Y, but positive sentiment between X and Y. In these cases, a neutral sentiment is to be put instead.

		\item Separate good and bad news from the sentiment expressed. We should NOT annotate the good versus bad content of the news. E.g. if the news talks about 7000 people being killed by a bomb, the news is factual/objective (neutral sentiment), even if there is negative news content (7000 people being killed).

		\item Expressions of attitude or intent: ``EU is willing to make efforts to prevent the terrorist threats from becoming a crisis" (This shows positive attitude, i.e. positive sentiment); On the other hand, the sentence ``EU sent help to the earthquake-affected Aquila citizens" is objective from a sentiment point of view (neutral sentiment).

		\item Should there be both positive and negative statements in the snippet, please consider the statement to be objective (OBJ) (Neutral sentiment). (strictly speaking, it would be subjective, but balanced.

		\item It is certain that there will be many cases of doubt. In these cases, simply annotate the article with a neutral sentiment.
	\end{enumerate}
	
	Additional rules as stated below, were constructed by the proponents based on the results of their annotations on a sample set of 20 articles per session. 

	\begin{enumerate}
		\item Regardless of the reputation the people involved have, do not consider the article as negative or positive unless a negative or positive issue concerning or affecting the person is being reported.

		\item Consider the issue the title of the news is pertaining to, not the issues that come after or before it, or are correlated to it.

		\item In tagging the sentiments of articles, only consider the scope being defined by the news title.

		\item If any of the following holds true, then the article can be tagged as positive: (1) Does the article contain reports regarding the improvement of a previously reported issue/event? (2) Does the article contain reports regarding the insurance of public safety? (3) Does the article contain reports that are beneficial to the country?

		\item If the article either contains reports regarding the worsening of a previously reported issue/event, the compromise of public safety, or concerns that are destructive to the country, then the article can be considered as negative. 
	\end{enumerate}
	
	At every session, the preliminary guidelines were used as a basis and after every session, the Kappa score was computed. The guidelines were revised if the Kappa score did not equal to 1 and the final guidelines were produced after a Kappa score of 1 was achieved.
		
	To ensure a Fleiss Kappa score of 0.6, articles that produce disagreement will be removed one by one until the desired Fleiss Kappa score is achieved. If the Kappa score is still not met after the removal of 200, the data set will be left as is, and the Fleiss Kappa score kept below 0.6.

	The final sentiment to be used in the training of all classifiers will be taken from the annotations made by the validator.

\subsection{Pre-processing and Filtering of the Corpus}
	English and Tagalog stop words were removed from the corpus. The quotation marks placed at the very beginning of the article and immediately after the article ends were removed. This is so that the whole article will not be removed during the next phase. Since \citeauthor{taboada2011lexicon} \citeyear{taboada2011lexicon} has stated that irrealis markers, specifically those within quotation marks, are words or phrases that do not contribute to the classification of a text or do not alter the sentiment value of the word it is modifying, the next phase focused on removing all words and phrases within quotation marks (`` ") from the corpus. In the case of news articles, because words or phrases commonly found within parentheses are mostly abbreviations, all words and phrases enclosed in parentheses (()) were also deleted from the corpus.

\subsection{Creation of the Lexical Resources}
	The Semantic Orientation Calculator (SO-CAL) lexicon by \citeauthor{taboada2011lexicon} \citeyear{taboada2011lexicon} is segregated by several part-of-speech tags namely: adjective, adverb, intensifiers, noun, and verbs. Each datum within the lexicon is tagged with a corresponding value called the SO value which determines how a semantic term will contribute to the overall sentiment orientation of a sentence or phrase.

	The adjectives, adverbs, nouns, and verbs within SO-CAL is from the range [-5, +5]. Figure 4.2 shows sample data for adjectives.
	\begin{figure} [h!]
       \includegraphics[width=1.0\textwidth]{socal.png}      
       \caption{The adjectives are within the range of [-5, +5]}
        \label{fig:socal}
    \end{figure}
	
    Unfortunately, the existing SO-CAL lexicon is only available for the English language. For the purpose of this experiment, the existing SO-CAL lexicon was translated to Filipino. Every word in the SO-CAL lexicon was translated through the online translator Google Translate (translate.google.com). Its translation was accepted if it was able to fully translate a word into Filipino. For example, the word ``mega-success" from the original English SO-CAL was translated as ``mega-tagumpay". Since prefix ``mega-" was not translated to its Filipino equivalent, that machine translation was discarded.

	Furthermore, translated words which produced duplicates were removed. 

	A problem faced with this approach is on how the machine translator had difficulty translating the SO-CAL adverbs since it ends in -ly (e.g. slowly, gently, deftly, etc.). Therefore, the problematic words were processed in order to exclude the suffix -ly. This turns the word in question into an adjective. However, adjectives such as ``gently" were manually transformed into ``gentle" since removing its ``-ly" would not result to a valid word. The attempt at a proper translation was then conducted once more. The resulting Filipino adjective word was then manually transformed to a Filipino adverb. The results of this process were verified through the use of Filipino-English dictionary by Leo James English. 

	Lastly, the English words that were not successfully translated by the machine translator was manually translated by the proponents through the aid of the aforementioned dictionary. The words which did not have a Tagalog equivalent in the dictionary were simply ignored. 
	The remaining English words which were left unprocessed and did not have a grammatically correct counterpart in Tagalog were removed from the Tagalog lexicon.

    The remaining English words which were left unprocessed and did not have a grammatically correct counterpart in Tagalog (e.g. the English word ``mega success" which was electronically translated to ``mega tagumpay") were removed from the Tagalog lexicon.

    A separate set of Filipino valence shifters was created by considering various Filipino grammar resources. The valence shifters included in the set were translated in order to get their corresponding weights. In the case that duplicates in translation occurred, the average of the weights was assigned to the valence shifter instead. Weights of the valence shifter was initially based on the SO-CAL lexicon which is for English, and was validated by a linguist later.

    Finally, the wildcards found in the SO-CAL lexicon were simply ignored and was no longer included in the conversion. 

    The SO-CAL assigns the sentiment value of a term through the use of the following equation:
	\begin{center}
		$t \ast (a + s) = v$ \\
		where $t$ denotes the value of the word being modified,
		$a$ is a constant value of 100\%,
		$s$ is the weight of the sentiment shifter multiplied by 100, and
		$v$ is the modified sentiment value of the term $t$.
	\end{center}
	
	Table \ref{tab:intensifiers} shows the weights attached to the intensifiers within the translated SO-CAL. As can be seen, the intensifier is used alongside an adjective.

	\begin{table}[ht!]
		\centering
		\caption{Table of Intensifier} 
		\label{tab:intensifiers}
		\begin{tabular}{|l|l|}
		\hline
		\multicolumn{2}{|c|}{\textbf{Word before is an intensifier}} \\ \hline
		\multicolumn{1}{|c|}{tunay na} & \multicolumn{1}{c|}{1.5} \\ \hline
		ubod ng & 2 \\ \hline
		masyadong & 1.5 \\ \hline
		masyado na & 1.5 \\ \hline
		totoong & 1.5 \\ \hline
		totoo na & 1.5 \\ \hline
		\multicolumn{2}{|l|}{} \\ \hline
		\multicolumn{2}{|c|}{\textbf{Prefix intensifier}} \\ \hline
		napaka & 2 \\ \hline
		pinaka & 2 \\ \hline
		\end{tabular}
	\end{table}
	
    Given an input string ``pinakamaganda", stemming is first performed in order to get the root word ``ganda". The root word is then checked within the translated SO-CAL lexicon in order to determine the input text\textsc{\char13}s initial weight. Since the word contains the substring ``pinaka", realizing that the substring is an intensifier, the program will look for the substring’s weight in the list of valence shifters and use the value to get the total weight of the word.
	
	\begin{center}
		\say{Ito ang pinakamagandang ngiting nakita ko sa taong lubhang mahirap.}\newline
	\end{center}
	=0 + 0 + (100\%+100\%)* 4 + 1 + 0 + 0 +0 +0 + (100\% + 40\%)* -2\newline
	=0 + 0 + 8 + 1 + 0 + 0 + 0 +0 - 2.8\newline
	=6.2
	
	\begin{figure} [h!]
       \includegraphics[width=1.0\textwidth]{socal_lex.jpg}      
       \caption{The following example shows the use of the translated SO-CAL lexicon in order to determine a sentence’s polarity. }
        \label{fig:socal_lex}
    \end{figure}

	
\section{Feature Extraction}
    The articles gathered for the corpus are written in Filipino. These articles went through the feature extraction process. The annotated articles were evaluated on a document-level to ensure that the final sentiment pertains to the whole article.
  	
    Computation of the TF-IDF was performed in order to produce a bag of words containing the relevant terms within the corpus. This method falls under the corpus-based keyword selection approach, where the terms that achieve the highest TF-IDF score in the overall corpus are selected as the keywords \cite{ozgur2005text}. A program to compute for the TF-IDF of all words inside the corpus was implemented and the resulting words were sorted in decreasing order. During the training phase, each article was checked for the presence or absence of the features within the bag of words produced by the TF-IDF computation. The extracted features are within the range of 10\% to 40\% of the list, with intervals of 5\%, producing 6 features from the computation of the TF-IDF: 10\%, 15\%, 20\%, 25\%, 30\%, 35\%, and 40\%. 

    The n-grams of the training set were also taken in order to come up with the relevant phrases in the corpus. The implementation used by \citeauthor{nagao1994new} \citeyear{nagao1994new} in their study of new methods for n-gram statistics of large numbers was used in order to come up with an n-gram extraction tool. Operators such as the comma (,), period (.), colon (:), semi-colon (;), and forward-slash (/) found in between a group of words or phrases were used in order to separate the mentioned words or phrases into different n-grams. N configurations of 2 to 5 were used in the extraction, including only the n-grams occurring at least 5, 10, 15, 20, and 25 times throughout the whole corpus. Twenty features were taken through this described configuration, broken down into 5 features extracted per n value.

    The relevance of certain parts of speech was also identified using the part of speech tagging of the training data. The Stanford Log-linear Part-of-Speech Tagger \cite{toutanova2003feature} was trained using data taken from \citeauthor{rabo2004tpost}\textsc{\char13}s study \citeyear{rabo2004tpost} regarding a template-based, n-gram part-of-speech tagger for Tagalog in order to come up with a POS tagger for the Filipino language. The POS features were extracted by considering part of speech element sequences of length 2 to 5 that are frequently occurring in the corpus. A sequence is considered frequently occurring if it appears at least 5, 10, 15, 20, and 25 times throughout the corpus. A total of 20 features were extracted through this configuration. The total features extracted are composed of 5 feature sets per sequence length value.
	
    Presence of valence shifters such as ``lubhang", ``masyadong", and “ubod ng” was also considered as they affect the sentiment of certain words and phrases. A list of valence shifters was produced through the use of the SO-CAL lexicon which was translated to Filipino. Weights for the intensifiers were assigned by grouping the words based on their respective degrees (comparative and superlative) and assigning weights to the words corresponding to their group. Words under the superlative group were assigned a weight of 2, or were given an intensification power of 200\%, while comparative words were given a weight of 1.5 or 150\% intensification power.
	
	The final weights given to the intensifiers were determined through the consultation of a linguist. Different values for the intensifiers were experimented on and were validated through the use of 25 sentences having multiple aggregates of intensifiers. For every time a certain intensification value was assigned to each group, the weights of the test sentences were computed and validated by the linguist. Validation stopped once the linguist was satisfied with the resulting sentiment value of each test sentence. Examples of test sentences are given below:

	\begin{enumerate}
		\item Siya ang hindi pinakakahanga-hangang babae.
		\item Nakakainis ang nangyaring pinakamalaking patayan sa baryo.
		\item Ang suspek ay nakatakas sa pinakamabilis na paraan.
		\item Hindi ito ang pinakapangit kong nabili na gamit.
		\item Ang kanyang nabili na pagkain ay hindi mahal ngunit hindi masarap.
	\end{enumerate}

	Since the exact value of the linguist-assigned weights could not be matched by the SO-CAL-computed weights regardless of the value assigned to the intensifiers, the intensification values resulting in weights having the most number of equal or close matches to the linguist assigned weights were used.
	
	Negations were also used as a feature by determining the presence of n-grams with negations attached to them. Additionally, the amount of n-grams with negators were also analyzed in determining patterns found in texts having sentiments. This feature was extracted by taking all n-grams with n configurations of 2 to 5, occurring at least 5 to 25 times (having intervals of 5) in the corpus. A total of 20 features were extracted, broken down into 5 features extracted per n value.
	
	The features produced in this module served as an input for the classifiers to use.

\section {Feature Selection}   	
	Features that were extracted based on different methods were selected depending on the results produced by the classifiers after using the features. All features produced by the TF-IDF, n-gram, negation, and sentence pattern were grouped together and evaluated by each classifier, and the best feature for each group was determined by considering metrics such as accuracy, f-score, recall and precision. 
	
	For the Na\"{\i}ve Bayes classifier, best results were produced by taking 30\% of the TF-IDF to come up with the bag of words to use. Additionally, the feature selected from the n-gram group is 3-grams occurring at least 10 times throughout the corpus. Most optimal results were produced by extracting 2-grams occurring 5 times for negation, while the sequences of length 5 occurring 25 times produced the best results in the sentence pattern group.
	
	The model produced by the Maximum Entropy classifier performs best using 40\% of the list produced by the TF-IDF calculation. Negations of length 2 occurring 20 times within the corpus were also selected.
	
	The K-Nearest Neighbor classifier produced best results using 20\% of the words produced by the TF-IDF. The n-gram configuration of 4 for the value of n and 5 for the n-gram frequency was the most optimal. Using negations was most effective on the classifier by only considering 2-grams appearing 10 times. The feature selected from the sentence pattern group was 3-grams with a frequency of 25.
	
	For the Artificial Neural Network classifier, using 30\% of the TF-IDF to come up with the bag of words to use was the most optimal. The selected negation feature to use is a 2-gram appearing at least 10 times throughout the corpus.
	
	Support Vector Machines produced the best results by using the words taken from 15\% of the TF-IDF list, and using 3-grams occurring at least 20 times for the n-grams. Bigrams occurring 20 times in the corpus showed best results under the negations group, while taking 4-grams with frequency of 25 was the most ideal for the sentence pattern feature.

	In summary, a total of 20 features were selected, one for each classifier per feature group as shown below:

	\begin{landscape}
	\begin{table}[ht]
		\centering
		\caption{Features Selected}
		\label{my-label}
		\resizebox{1.3\textwidth}{!}{
		\begin{tabular}{|l|l|l|l|l|}
		\hline
		 & \multicolumn{4}{c|}{Features} \\ \hline
		\multicolumn{1}{|c|}{Classifiers} & \multicolumn{1}{c|}{TF-IDF} & \multicolumn{1}{c|}{N-gram} & \multicolumn{1}{c|}{Negation} & Sentence Pattern \\ \hline
		Naive Bayes & 70\% & \begin{tabular}[c]{@{}l@{}}3-gram occuring\\ at least 10 times\end{tabular} & 2-gram occuring at least 5 times & \begin{tabular}[c]{@{}l@{}}Sequence of 5 tags\\ occurring at least 10 times\end{tabular} \\ \hline
		Maximum Entropy & 70\% & \begin{tabular}[c]{@{}l@{}}3-gram occurring\\ at least 10 times\end{tabular} & 2-gram occurring at least 5 times & \begin{tabular}[c]{@{}l@{}}Sequence of 5 tags\\ occurring at least 10 times\end{tabular} \\ \hline
		K-Nearest Neighbor & 80\% & \begin{tabular}[c]{@{}l@{}}4-gram occurring\\ at least 5 times\end{tabular} & 2-gram occurring at least 10 times & \begin{tabular}[c]{@{}l@{}}Sequence of 3 tags\\ occurring at least 25 times\end{tabular} \\ \hline
		\begin{tabular}[c]{@{}l@{}}Artificial\\   Neural Network\end{tabular} & 80\% & \begin{tabular}[c]{@{}l@{}}4-gram occurring\\ at least 5 times\end{tabular} & 2-gram occurring at least 10 times & \begin{tabular}[c]{@{}l@{}}Sequence of 3 tags\\ occurring at least 25 times\end{tabular} \\ \hline
		\begin{tabular}[c]{@{}l@{}}Support\\   Vector Machines\end{tabular} & 85\% & \begin{tabular}[c]{@{}l@{}}3-gram occurring\\ at least 20 times\end{tabular} & 2-gram occurring at least 20 times & \begin{tabular}[c]{@{}l@{}}Sequence of 4 tags\\ occurring at least 25 times\end{tabular} \\ \hline
		\end{tabular}}
	\end{table}
	\end{landscape}


	In order for the classifiers to use the data set produced by the selected features, the 4 best features per classifier obtained earlier require combination. Aggregation of the 4 feature sets assigned to each classifier after selection was performed by producing 15 unique combinations of the feature sets. The first four combinations (TF-IDF, n-gram, negation, and sentence pattern) were removed because the results of these combinations were already taken previously. The 11 combinations which remained were used in order to produce training sets for the classifiers. After training, the models produced were tested in order to obtain the best aggregated feature set for each classifier. Metrics such as accuracy, f-score, recall, and precision were used again in order to evaluate the results of the experiments. Results were analyzed in order to observe how features and data sets affect the performance of the model.
	Once the best combination per classifier was produced, the feature sets underwent further selection through the use of the tool WEKA. Correlation-based Feature Subset Selection was used as the evaluator, and forward Best First or Genetic Search as the search method. The Correlation-based Feature Subset Selection evaluates the worth of a subset of features by considering the individual predictive ability of each feature along with the degree of redundancy between the features, while the Best First algorithm determines the most appropriate features to include by searching through the space of attribute subsets by greedy hillclimbing augmented with a backtracking facility. On the other hand, Genetic Search selects features by performing a search using the simple genetic algorithm proposed in the book Genetic Algorithms in Search \cite{goldberg1988genetic}. The search algorithm to be used will be decided by comparing the results produced by both algorithms and selecting the algorithm that produces better results.

	Feature reduction will only be applied in the case that an increase in the quality of the results is achieved. If results after feature reduction are poorer, the original data set will be retained for use. Results of all 5 classifiers before and after feature reduction are presented as follows:

	The Na\"{\i}ve Bayes classifier showed best results using a combination of feature sets from the n-grams and TF-IDF. Further selection caused the initial data set having 813 features to only have 313 remaining features. The search algorithm used for this feature set is the Genetic Search.


	\begin{table}[ht]
		\centering
		\caption{Results produced by the Na\"{\i}ve Bayes classifier using a reduced feature set.}
		\label{nb-results}
		\resizebox{1\textwidth}{!}{
		\begin{tabular}{ll|l|l|l|l|l|l|l|l|l|}
		\cline{3-11}
		 & \multicolumn{1}{c|}{} & \multicolumn{3}{c|}{F-score} & \multicolumn{3}{c|}{Recall} & \multicolumn{3}{c|}{Precision} \\ \hline
		\multicolumn{1}{|c|}{Feature Set} & \multicolumn{1}{c|}{Accuracy} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} \\ \hline
		\multicolumn{1}{|l|}{Original} & 40.77\% & 0.2660 & 0.3605 & 0.5125 & 0.1923 & 0.3232 & 0.7077 & 0.4310 & 0.4078 & 0.4017 \\ \hline
		\multicolumn{1}{|l|}{Reduced} & 51.22\% & 0.4208 & 0.1473 & 0.6945 & 0.4469 & 0.0953 & 0.8256 & 0.3976 & 0.3233 & 0.5994 \\ \hline
		\end{tabular}}
	\end{table}

	The model produced by the Maximum Entropy classifier performed best using a combination of feature sets from the n-grams and the TF-IDF. Using Genetic Search in order to reduce the feature set further will cause the initial data set to have 500 lesser features.
	
	\begin{table}[ht]
		\centering
		\caption{Results produced by the Maximum Entropy classifier using a reduced feature set.}
		\label{maxent-results}
		\resizebox{1\textwidth}{!}{
		\begin{tabular}{ll|l|l|l|l|l|l|l|l|l|}
		\cline{3-11}
		 &  & \multicolumn{3}{c|}{F-score} & \multicolumn{3}{c|}{Recall} & \multicolumn{3}{c|}{Precision} \\ \hline
		\multicolumn{1}{|c|}{Feature set} & \multicolumn{1}{c|}{Accuracy} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} \\ \hline
		\multicolumn{1}{|l|}{Original} & 40.77\% & 0.2660 & 0.3605 & 0.5125 & 0.1923 & 0.3231 & 0.7077 & 0.4310 & 0.4078 & 0.4017 \\ \hline
		\multicolumn{1}{|l|}{Reduced} & 51.09\% & 0.4689 & 0.2526 & 0.6201 & 0.3892 & 0.1992 & 0.7904 & 0.5896 & 0.3451 & 0.5101 \\ \hline
		\end{tabular}}
	\end{table}

	The K-Nearest Neighbor classifier produced most optimal results after using the data set from the combination of the n-gram and negation feature sets. The original feature set produced by the combination was further reduced in order to come up with a data set that has 418 lesser attributes. Reduction was accomplished through the use of the Genetic Search algorithm.

	\begin{table}[ht]
		\centering
		\caption{Results produced by the K-Nearest Neighbor classifier using a reduced feature set.}
		\label{knn-results}
		\resizebox{1\textwidth}{!}{
		\begin{tabular}{ll|l|l|l|l|l|l|l|l|l|}
		\cline{3-11}
		 & \multicolumn{1}{c|}{} & \multicolumn{3}{c|}{F-score} & \multicolumn{3}{c|}{Recall} & \multicolumn{3}{c|}{Precision} \\ \hline
		\multicolumn{1}{|c|}{Feature set} & \multicolumn{1}{c|}{Accuracy} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} \\ \hline
		\multicolumn{1}{|l|}{Original} & 35.13\% & 0.1641 & 0.4043 & 0.4221 & 0.1231 & 0.4308 & 0.5 & 0.2462 & 0.381 & 0.3652 \\ \hline
		\multicolumn{1}{|l|}{Reduced} & 41.14\% & 0.3234 & 0.2367 & 0.5635 & 0.3496 & 0.2018 & 0.5879 & 0.301 & 0.2862 & 0.5411 \\ \hline
		\end{tabular}}
	\end{table}


	Similar to the K-Nearest Neighbor classifier, best performance is also achieved using the combination of features extracted from the n-gram, negation and TF-IDF. The Artificial Neural Network’s data set was reduced by Genetic Search in order to come up with a feature set having 238 attributes versus the original one having 656 attributes.

	\begin{table}[ht]
		\centering
		\caption{Results produced by the Artificial Neural Network classifier using a reduced feature set.}
		\label{ann-results}
		\resizebox{1\textwidth}{!}{
		\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|}
		\hline
		 &  & \multicolumn{3}{c|}{F-score} & \multicolumn{3}{c|}{Recall} & \multicolumn{3}{c|}{Precision} \\ \hline
		\multicolumn{1}{|c|}{Feature set} & \multicolumn{1}{c|}{Accuracy} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} \\ \hline
		\multicolumn{1}{|l|}{Original} & 35.13\% & 0.1641 & 0.4043 & 0.4221 & 0.1231 & 0.4308 & 0.5 & 0.2462 & 0.381 & 0.3652 \\ \hline
		\multicolumn{1}{|l|}{Reduced} & 47.39\% & 0.5006 & 0.0379 & 0.5483 & 0.4731 & 0.0203 & 0.7086 & 0.5314 & 0.2778 & 0.4479 \\ \hline
		\end{tabular}}
	\end{table}
	
	The model produced by the Support Vector Machine performed the best using data sets taken from the combination of n-grams and negations. From a data set containing 385 attributes, WEKA-aided feature selection was able to reduce the attributes to only 69. Genetic Search was used to reduce this classifier\textsc{\char13}s feature set.

	\begin{table}[ht]
		\centering
		\caption{Results produced by the Support Vector Machine classifier using a reduced feature set.}
		\label{svm-results}
		\resizebox{1\textwidth}{!}{
		\begin{tabular}{ll|l|l|l|l|l|l|l|l|l|}
		\cline{3-11}
		 & \multicolumn{1}{c|}{} & \multicolumn{3}{c|}{F-score} & \multicolumn{3}{c|}{Recall} & \multicolumn{3}{c|}{Precision} \\ \hline
		\multicolumn{1}{|c|}{Feature set} & \multicolumn{1}{c|}{Accuracy} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} & \multicolumn{1}{c|}{Pos} & \multicolumn{1}{c|}{Neg} & \multicolumn{1}{c|}{Neut} \\ \hline
		\multicolumn{1}{|l|}{Original} & 33.33\% & 0 & 0.0152 & 0.4981 & 0 & 0.0077 & 0.9923 & 0 & 0.5 & 0.3325 \\ \hline
		\multicolumn{1}{|l|}{Reduced} & 49.22\% & 0.3546 & 0 & 0.6605 & 0.3075 & 0 & 0.9323 & 0.4187 & 0 & 0.5115 \\ \hline
		\end{tabular}}
	\end{table}

	\begin{table}[ht]
		\centering
		\caption{A summary of the number of attributes before and after further feature selection}
		\resizebox{1\textwidth}{!}{
		\begin{tabular}{|l|l|l|}
		\hline
		\multicolumn{1}{|c|}{Classifier} & \multicolumn{1}{c|}{Before WEKA-assisted feature selection} & \multicolumn{1}{c|}{After feature selection} \\ \hline
		Na\"{\i}ve Bayes                      & 813                                                         & 313                                          \\ \hline
		Maximum Entropy                  & 813                                                         & 313                                          \\ \hline
		K-Nearest Neighbor               & 656                                                         & 238                                          \\ \hline
		Artificial Neural Network        & 656                                                         & 238                                          \\ \hline
		Support Vector Machine           & 385                                                         & 69                                           \\ \hline
		\end{tabular}}
		\label{my-label}
	\end{table}

\section{Classifiers}
    The existing methodologies for sentiment classification are generalized into two approaches: (1) rule-based classifiers and (2) machine learning-based classifiers

\subsection{Rule-based Classifier}
    Rule-based classifiers were implemented for the experiment, each with their own set of rules for classification. Each set of sentiment analyzing rules were manually formulated based from the features listed in section 4.3. The first set was based on the rules of SO-CAL as described by \citeauthor{taboada2011lexicon} \citeyear{taboada2011lexicon}. While the second was based on the term frequency model used by \citeauthor{bakliwal2012mining} \citeyear{bakliwal2012mining}. Sample rules from each set are defined below:

    A.  SO-CAL-based rules:
    \begin{enumerate}
    	\item Intensifiers are assigned percentage values instead of integer ones.
		\item Switch negations or negators such as the word ``not" either increase or decrease the polarity value of the word it modifies by 4.
		\item Sentiment terms being modified by irrealis markers are to be disregarded by performing irrealis blocking.
    \end{enumerate}

    B.  Term frequency model-based rules:
    \begin{enumerate}
        \item For every unigram in the text, count the number of positive and negative occurrences the text has.
        \item Divide both values obtained by counting the positive and negative occurrences in the text by the size of the text the unigram has occurred in. The resulting value will define the probability of the word being positive and negative respectively.
        \item Sum up the positive and negative probability scores of all the unigrams.
        \item Use their difference (positive - negative) to find the overall score of the article.
        \item If the score is greater than 0, the article is classified as positive, and if the score is less than 0, then the article is classifier as negative. A score of 0 would denote that the article has a neutral sentiment.
    \end{enumerate}
   The rules presented above are subject to change as other more rules will still be formulated.

\subsection{Machine Learning-Based Classifiers}
	Five classifiers namely: Support Vector Machines (SVM), Na¨ıve Bayes, Maximum Entropy, K-Nearest Neighbor (KNN), and Artificial Neural Network (ANN) were utilized in this study. The machine learning-based classifiers were trained using n-grams and bag of words. By obtaining relevant terms in the document, and determining how their presence or absence affected the sentiment classification of a certain document, training sets for the five classifiers to use were produced. 
	
	The data sets used in the training and testing of the classifiers are similar to those used by Pak and Paroubek (2010) and Cagampan (2015) in their respective studies. Their data sets were proven to have yielded acceptable accuracies. The presence and absence of the features selected were utilized in order to determine the article's sentiment.


\subsection{Result Aggregation}
	Various methods to aggregate the results of the classifiers were also experimented on.

	The voting system used by the multi-classifier system \citeauthor{das2007yahoo} \citeyear{das2007yahoo} created was considered in the aggregation of results. By considering the most number of votes under each sentiment classification, the final sentiment of the text was obtained. A majority vote is defined as the highest number of a certain vote count for a certain sentiment. In the case of equal vote counts for a certain sentiment classification, no sentiment will be assigned to the text. This configuration caused the classified articles to be less than the amount of articles included in the test set.

	A variation of the voting system \citeauthor{das2007yahoo} \citeyear{das2007yahoo} employed was also implemented. The variation included the assignment of weights to each vote made by the classifiers in the aggregation of classification results. The weights were determined during the training phase by comparing each classifier’s accuracy to that of the gold standard test set before the actual voting takes place. The better performing classifiers during the training phase would have a weighted vote, therefore making their decisions more influential on the result \cite{tulyakov2008review}. Ten percent of the accuracy produced by the classifier during training will be taken in order to serve as the multiplier weight of the vote the classifier will be casting.

	Lastly, classifier results were combined through the use of the stacking framework. The base-level classifiers are the feature-trained classifiers from the previous module, while the meta-level classifier implemented was the SVM. Using the sentiment results produced by the base-level classifiers as input, the meta-level classifier produced a single and final sentiment result as output.
	
	The results of the 3 ensemble classifiers will be compared against each other in order to get the best aggregation method and the most optimal multi-classifier model. The most optimal ensemble classifier will also be used to determine the overall sentiment of the article. Furthermore, results of the classifier aggregation will be analyzed individially in respect to the attributes of the data and feature sets in order to determine the characteristics of the language that may have affected the results of the ensemble classifiers.